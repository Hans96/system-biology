---
title: "A Mathematical Model of Cell Cycle Dysregulation Due to Human Papillomavirus Infection"
author: "Rik van de Pol & Hans Zijlstra"
date: "June 14, 2019"
output: pdf_document
---


```{r, echo=FALSE}
knitr::include_graphics("Voorpagina.jpg")
```

\newpage

\newpage
\tableofcontents

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


```{r, echo=FALSE}
library(deSolve)
```

\newpage

#1 Introduction

The Human paillomaviruses (HPVs) are a group of sexually transmitted viruses. The viruses are divided in two groups, The group that when infected gives a high risk of getting cancer and the group were the risk of getting cancer  is low.$^1$ Research on HPV has been done extensively. To get a better understanding of the effect of HPV on infected cells, the research paper A Mathematical Model of Cell Cycle Dysregulation Due to Human Papillomavirus Infection, has been chosen to (partially) recreate. 

HPVs infects mucosal epithelium. Infected cells can undergo lysis. HPVs produce a specific proteins (E7) that bind to cell cycle regulatory proteins. One important area where these proteins bind is to the retinoblastoma tumor suppressor protein($RB$). E7 protein is also able to trigger S phase progression $^2$. 

A Mathematical Model of Cell Cycle Dysregulation Due to Human Papillomavirus Infection obtains insight in the process of cell regulation when cells are in infected with HPV's. $RB$ inactivation is a tightly controled mechanism that, together with growth factor, stimulate cells to divide. HPV E7 protein affects this regulatory process by decreasing growth factors. The article developed  mathematical model that examines the role of $RB$, growth factor concentration and E7  on cell cycle progression.$^1$ The article gives the following prediction of the created mathmatical model: "high $RB$ binding affinity and E7 concentration accelerate the G1 to S phase transition and weaken the dependence on growth factor. This model thus captures a key step in high-risk HPV oncogenesis".$^1$ This article will focus on the mathmatical model and try to recreate the the outcome in the article. To do this the following research question has been specified:

Does high $RB$ binding affinity and E7 concentrationdue to HPV accelerate the G1/S phase transition and does it weaken the dependence on growth factor?

\newpage

#2 Materials and methods

This chapter describes the material and methods used to recreate the model when HPV infects cells. First the mechanisms of the biological model will be explained when the cells is uninfected. Following up the biological model for infected cells. Finally the assumption and estimates for this model will be explained. 


##2.1 Biological model uninfected cell


``` {r, echo=FALSE, fig.width=7, out.width = "450px",fig.height=6,fig.cap="\\label{fig:model1}Uninfected model", echo=FALSE} 
knitr::include_graphics("11538_2017_299_Fig1_HTML.jpg")
```

Two biological models have been set forth in the article. A biological model for infected and uninfected basal cel is made that shows how HPV $E7$ proteins affect the mechanism of the cel.The figure shown above shows the model for low risk HPV $E7$. Here $E2F$, a key transcription factor, is inhibited until the end of the G1 phase.$^3$ $E2F$ plays an important rol in cel proliferation. It is assumed that the $E2F$ is bound to unphosphorylated $RB$ that inhibits transcription. $RB$ must be phosphorylated to release $E2F$ from repression. Phosphorylation of $RB$ takes place by activating specific cyclin-dependent kinases ($CDKs$).$^4$ 

For the cell to go through the G1 fase of the cell cycle it is dependent on growth factor thats stimulates the transcription of Cyclin D6. A complex is formed between Cyclin D and $CDK4/6$ that activates kinase. Cyclin complex $D$:$CDK4/6$ ($CD$) phosphorylates $RB$ ($R$) and the $RB$, $E2F$ complex. The rate at which phosphorylation happens is defined by rates $P1u$ and $P1c$ that causes $RB$ to hypophosphorylate. $RB$,$E2F$ complex will unbind $E2F$ at the rates $K1$ and $K2$, they bind $E2F$ with rates $k1$ and $k2$. Free $E2F$ will cynthesize cyclin $E$ with a rate defined as $a2u$. Cyclin E forms complex with $CDK2$ that activates kinase. This formed complex ($CE$) phosphorylates hyphosphorylated $RB$ with rates $P2u$ and $P2c$ that causes $RB$ to become hyperphosphorylated. When hyperphosphorylated the inactivation and release of $RB$ from $E2F$ will take place, This creates a positive feedback since transcription of cyclin E will drive further $RB$ inactivation $^7$ 


\newpage
## 2.2 Biological model infected cell

```{r, echo=FALSE, fig.width=7, out.width = "350px",fig.height=6,fig.cap="\\label{fig:model2}Infected model", echo=FALSE} 
knitr::include_graphics("11538_2017_299_Fig2_HTML.jpg")
```

The difference between uninfected and infected basal cells is the incorporation of $E7$ is a protein secreted by HPV that affect the mechanisms of the cell proliferation 2 . $E7$ binds to $RB$ unphosphorylated $RB$ with different rates defined as $K3$ and $K5$ (see figure 1). $E7$ also binds to hypophosphorylated $RB$ with rates $k4$ and $k6$ and it unbinds hypophosphorylated $RB$ with rates $K4$ and $K6$. When $E7$ binds to $RB$ it bypasses the $RB$ phosphorylation need to release $E2F$. This causes $E2F$ to realease from $RB$,$E2F$,$E7$ complex with a rate of $K7$. It released from phosphorylated $RB$, $E2F$, $E7$ complex with rate $K8$ 


\newpage


##2.3 Estimates and assumptions

Creation of the model comes with multiple assumptions about concentration values and different biological states usually backed by multiple papers and reports. An important assumption for the working of the model is that all $E2F$ in the initiall state is bound to unphophorylated $RB$, thereby inhibiting the transcription factor. 

The following spicific assumptions have been made regarding the model:

1.The binding of cyclin to $CDK$ has not been explicitly modelled due to the fact that CDKs are avaiblabe in excess compared to the amount of cyclins. 

2.Cyclin E transcription has been determined with the Michaelis-Menten dependence on $E2F$. It is assumed that the rate is proportional to the probability that $E2F$ is bound to Cyclin E gene promoter site. 

3.Uncatalyzed Dephosphorylation of $Rp$, $RpE$ and $Rpp$ may occur 

In infected Basal Cells it is assumed that $CD$ is able to phosphorylate $RV$ and $REV$ and $CE$ is able to do the same to $RpV$ and $RpEV$

Uncatalyzed dephosphrylation of $RpV$ and $RpEV$ may take place. 

###2.3.1 Initial and parameters estimates

Concentration values have been chosen under different biological conditions. In the following section these values are assumptions based on several biological conditions. $RB$ is being dephosphorylated by protein phosphatase 1 $^9$. Therefore in this model all the $RB$ is in unphosphorylated in the beginning of G1. It is assumed that the initial concentration of free unphosphorylated $RB$ ($R0$) is greater than the initial concentration of $E2F$bound unphosphorylated $RB$. Therefore $R0$ is set to 0.25 uM and $RE$ to 0.2. Using these values it is ensured that $E2F$ remains inactive for different amounts of growth factor concentrations, making sure $E2F$ does not become activated too early.  All other proteins at the start of G1 are small. Therefore all other initial conditions mentioned in figure are set to zero.

The decay rate of Cyclin D is between 20 and 30 minutes  With a half life of 28 minutes$^{10}$ degradation of Cyclin D has been set to (60ln2)/28 = 1.5 uM per hour. The same goes for the half life of Cyclin E around 30 minutes. Degradation of Cyclin E is (60ln2)/30 = 1.4 uM per hour. Estimation has been made where the disscociation constant $E2F$ and Cyclin E ($Ke$) is based on the disscociation constant of the Ebox Dna and the myc-max dimer that is in  the range of 0.11 to 0.21 uM Therefore $Ke$ is set to 0.153 uM $^{11}$.  

Phosphorylation rates of cyclin D and cyclin E have been based on posphorylated rates of extracellular signal regulated kinase here ERK here ERK is phosphorylated by MEK just as $RB$ is sequentially phosphorylated by Cyclin D and Cyclin E this happens in the range of 72 to 140 uM the phosphorylation rate has therefore been set to 80 uM $^{12}$. $RB$ is assumed to be phosphorylated at the same rate wehther $RB$ is unphosphorylated or not, this goes for the phosphorylation rates: $P1u$, $P1C$, $Pilu$, $Pi1c$, $P2u$, $P2c$, $Pi2u$, $Pi2c$. 

Dephosphorylation of ERK occurs in the range of 11 to 28 uM per hour $^{12}$ the model uses no enzym driven dephosphorylation which is lower. Therefore the dephosphorylation rate of hypophosphorylated $RB$ to be 0.5 uM per hour and hyperphosphorylated $RB$ to be 0.1 uM per hour, This assumes that  that hyperphosphorylated $RB$ is more stable than hypophosphorylated $RB$ $^{12}$.

Binding and unbinding ($RB$,$E2F$) $RB$ binds to $E2F$ with an on rate between 3254.4 and 3780 uM per hour $^13$. Therefore on rates ($k1$, $k2$) have been set to 3500 uM per hour. Here the assumption is made that unphosphorylated $RB$ and hypophosphorylated $RB$ bind to $E2F$ at the same rate. Thus the disscociation constant of $RB$ and $E2F$ is 0.0006. 

Binding and unbinding of $E7$ to $RB$ is a magnitude larger than the on rate described of $E2F$ described above. The on rates ($k3$, $k4$, $k5$,$k6$) are therefore set to be 35.000 uM per hour. Here the assumption is made that $E7$ binds to unphosphorylated $RB$ and hypophosphorylated $RB$ with the same rate $^{14}$. The off rates defined by ($K3$, $K4$, $K5$, $K6$)  are for high risk infection set to be 2 uM per hour. For low risk the same rates are set to 20 per hour since high risk has a ten fold higher binding affinity$^{15}$. ($K7$, $K8$) activation rate of $E2F$ is activated since high risk infection is faster binding $E7$ than the rate that $RB$ unbinds $E2F$ for this reason the activation rates have been set to 20 uM per hour

Two important parameters will have varying values, growth factor ($GF$) and $E7$ concentrations. The amount of growth factor for each is equal to the amount of transcription factor per cell, which is 10$^3$ to 10$^6$ molecules per cell.  

One micromolar equals $10^{-6}$ mol/L and the volume of a HeLa cell is 1.2 picoliters (pL), It is therefore estimated that there are 0.0014 to 1.4 uM of growth factor per cel. To explore the dynamics of the model a wide variety of $E7$ value is used. 

##2.4 material

Calculation of the cell cycle dysregulation due to Human papillomavirus will be simulated by using the programming language R version 3.3.3. The package deSolve will be used to calculate the differences in the earlier mentioned biological models. These values are of biological interest and calculated at different time values by using the function ODE. The ODE function solves a system of ordinary differential equations $^{17}$. In order to function properly the ode function requires four distinct parameters. The parameters that were provided to ODE are times,state,y and a custom made function. The times parameter represents the time that has past from when the function was iniated. The parameter state consists of variables that will be measured over time. Y are variables that do not change over time but have a direct effect on the parameter state. 

##2.5 Tables variables and parameters. 
Below the tables for the variables and parameters are displayed. For every variable in the table a description is provided. If relevant the amount and units are presented. 

**Table 1 Variable Definitions**

Variable  | Description
----------|-------------------------------
$CD$      | Cyclin D:CDK4/6
$R$       | Unphosphorylated $RB$
$Rp$      | Hypophosphorylated $RB$
$Rpp$     | Hyperphosphorylated $RB$
$RE$      | Unphosphorylated $RB$:E2F
$RpE$     | Hypophosphorylated $RB$p:E2F
$E$       | E2F
$CE$      | Cyclin E:CDK2
$RV$      | Unphosphorylated $RB$:E7
$RpV$     | Hypophosphorylated $RB$p:E7
$REV$     | Unphosphorylated $RB$:E2F:E7
$RpEV$    | Hypophosphorylated $RB$p:E2F:E7


\newpage

**Table 2 Parameter definitions and estimates**

Parameter                                          | Description                                        | Value         | Units
---------------------------------------------------|----------------------------------------------------|---------------|-------------------------------
$\alpha_1$                                         |Growth factor synthesis rate                        |1.0            |$\mathrm {h^{-1}}$            
$GF$                                               |Growth factor                                       |Varies         |$\mathrm {\mu M}$  
$E7$                                               |HPV E7 protein                                      |Varies         |$\mathrm {\mu M}$  
$\alpha_{2u}$                                      |CE synthesis rate                                   |0.4            |$\mathrm {\mu M\>h^{-1}}$
$\delta _{D}$                                      |CD decay rate                                       |1.5            |$\mathrm {h^{-1}}$ 
$\delta _{E}$                                      |CE decay rate                                       |1.4            |$\mathrm {h^{-1}}$ 
$K_{E}$                                            |E2F:DNA dissociation constant                       |0.153          |$\mathrm {\mu M}$ 
$k_{1}, k_{2}$                                     |Binding rate of $RB$ to E2F                           |3500           |$\mathrm {\mu M^{-1}\>h^{-1}}$
$k_{3}, k_{4},k_{5}, k_{6}$                        |Binding rate of E7 to $RB$                            |35,000         |$\mathrm {\mu M^{-1}\>h^{-1}}$
$K_{1}, K_{2}$                                     |Dissociation rate of $RB$ and E2F                     |2              |$\mathrm {h^{-1}}$
$K_{3}, K_{4}, K_{5}, K_{6}$                       |Dissociation rate of E7 and $RB$                      |HR: 2, LR: 20  |$\mathrm {h^{-1}}$
$K_{7}, K_{8}$                                     |E2F activation rate due to E7                       |20             |$\mathrm {h^{-1}}$
$\rho_{1u}, \rho_{1c}, \rho_{i1u}, \rho_{i1c}$     |Phosphorylation rate of unphosphorylated $RB$         |80             |$\mathrm {\mu M^{-1}\>h^{-1}}$
$\rho_{2u}, \rho_{2c}, \rho_{i2u}, \rho_{i2c}$     |Phosphorylation rate of hypophosphorylated $RB$       |80             |$\mathrm {\mu M^{-1}\>h^{-1}}$
$\rho_{d1u}, \rho_{d1c}, \rho_{di1u}, \rho_{di1c}$ |Dephosphorylation rate of hypophosphorylated $RB$     |0.5            |$\mathrm {h^{-1}}$
$\rho_{d2u}$                                       |Dephosphorylation rate of hyperphosphorylated $RB$    |0.1            |$\mathrm {h^{-1}}$

*HR high-risk, LR low-risk*

\newpage


##2.6 Formulas
Below the formulas that are used to calculate the different state are specified. 

(1) $\begin{aligned} \frac{\mathrm {d}[\mathrm{CD}]}{\mathrm {d}t}= & {} \alpha _{1}[\mathrm{GF}]-\delta _{D}[\mathrm{CD}] \end{aligned}$

(2) $\begin{aligned} \frac{\mathrm {d}[R]}{\mathrm {d}t}= & {} -k_{1}[R][E]+k_{-1}[RE]-\rho _{1u}[\mathrm{CD}][R]+\rho _{d1u}[Rp]-k_{3}[E7][R]\nonumber     \\&+\,k_{-3}[RV] \end{aligned}$

(3) $\begin{aligned} \frac{\mathrm {d}[Rp]}{\mathrm {d}t}= & {} \rho _{1u}[\mathrm{CD}][R]-\rho _{2u}[\mathrm{CE}][Rp]-k_{2}[Rp][E] +k_{-2}[RpE] \nonumber \\&-\,\rho _{d1u}[Rp]+\rho _{d2u}[Rpp]-k_{4}[E7][Rp]+k_{-4}[RpV] \end{aligned}$

(4) $\begin{aligned} \frac{\mathrm {d}[Rpp]}{\mathrm {d}t}= & {} \rho _{2u}[\mathrm{CE}][Rp]+\rho _{2c}[\mathrm{CE}][RpE]-\rho _{d2u}[Rpp]+\rho _{i2u}[\mathrm{CE}][RpV] \nonumber \\&+\,\rho _{i2c}[\mathrm{CE}][RpEV] \end{aligned}$

(5) $\begin{aligned} \frac{\mathrm {d}[RE]}{\mathrm {d}t}= & {} k_{1}[R][E]-k_{-1}[RE]- \rho _{1c}[\mathrm{CD}][RE] +\rho _{d1c}[RpE] \nonumber \\&-\,k_{5}[E7][RE]+k_{-5}[REV] \end{aligned}$

(6) $\begin{aligned} \frac{\mathrm {d}[RpE]}{\mathrm {d}t}= & {} \rho _{1c}[\mathrm{CD}][RE]+k_{2}[Rp][E]-k_{-2}[RpE] -\rho _{2c}[\mathrm{CE}][RpE]\nonumber \\&-\,\rho _{d1c}[RpE]-k_{6}[E7][RpE]+k_{-6}[RpEV] \end{aligned}$

(7) $\begin{aligned} \frac{\mathrm {d}[E]}{\mathrm {d}t}= & {} -k_{1}[R][E]+k_{-1}[RE]-k_{2}[Rp][E]+k_{-2}[RpE] \nonumber \\&+\,\rho _{2c}[\mathrm{CE}][RpE]+\rho _{i2c}[\mathrm{CE}][RpEV]+k_{-8}[RpEV]\nonumber \\&+\, k_{-7}[REV] \end{aligned}$

(8) $\begin{aligned} \frac{\mathrm {d}[\mathrm{CE}]}{\mathrm {d}t}= & {} \frac{\alpha _{2u}[E]}{K_{E}+[E]}-\delta _{E}[\mathrm{CE}] \end{aligned}$

(9) $\begin{aligned} \frac{\mathrm {d}[RV]}{\mathrm {d}t}= & {} k_{3}[E7][R]-k_{-3}[RV]-\rho _{i1u}[\mathrm{CD}][RV] +\rho _{di1u}[RpV] \nonumber \\&+\,k_{-7}[REV] \end{aligned}$

(10) $\begin{aligned} \frac{\mathrm {d}[RpV]}{\mathrm {d}t}= & {} \rho _{i1u}[\mathrm{CD}][RV]-\rho _{i2u}[\mathrm{CE}][RpV] -\rho _{di1u}[RpV]\nonumber \\&+\,k_{-8}[RpEV]+k_{4}[E7][Rp]-k_{-4}[RpV] \end{aligned}$

(11) $\begin{aligned} \frac{\mathrm {d}[REV]}{\mathrm {d}t}= & {} -\rho _{i1c}[\mathrm{CD}][REV]+\rho _{di1c}[RpEV] +k_{5}[E7][RE] \nonumber \\&-\,k_{-5}[REV]-k_{-7}[REV] \end{aligned}$

(12) $\begin{aligned} \frac{\mathrm {d}[RpEV]}{\mathrm {d}t}= & {} \rho _{i1c}[\mathrm{CD}][REV] -\rho _{i2c}[\mathrm{CE}][RpEV]-\rho _{di1c}[RpEV] \nonumber \\&-\,k_{-8}[RpEV]+k_{6}[E7][RpE]-k_{-6}[RpEV] \end{aligned}$

\newpage


```{r, echo=FALSE}
infected <- 10^-5
low.infected <- 10^-3
uninfected <- 0

highriskK <- c(2,2,2,2,2,2,20,20)
lowriskK = c(2,2,20,20,20,20,20,20)


parms <- function(GF=0.1, E7=0, K = lowriskK){ 
  return (c(
  a1 = 1.0,
  GF = GF, #
  E7 = E7, #
  a2u = 0.4,
  dD = 1.5,
  de = 1.4,
  KE = 0.153,
  k1 = 3500,
  k2 = 3500,
  k3 = 35000,
  k4 = 35000,
  k5 = 35000,
  k6 = 35000,
  K = K,
  P1u = 80,
  P1c = 80,
  Pi1u = 80,
  Pi1c = 80,
  P2u = 80,
  P2c = 80,
  Pi2u = 80,
  Pi2c = 80,
  Pd1u = 0.5,
  Pd1c = 0.5,
  Pdi1u = 0.5,
  Pdi1c = 0.5,
  Pd2u = 0.1
))
}


state <- c(
  CD = 0,
  R = 0.25,
  RE = 0.2,
  Rp = 0,
  Rpp = 0,
  RpE = 0,
  E = 0,
  CE = 0,
  RV = 0,
  RpV = 0,
  REV = 0,
  RpEV = 0
)

times <- seq(0,20,0.1)
calc.Variables <- function(t, state, parms) {
 with(as.list(c(parms, state)),
  {
     dCD <- (a1 * GF) - (dD * CD)

     
     dR <- (-k1 * R * E) + (K1 * RE) - (P1u * CD * R) + 
           (Pd1u * Rp) - (k3 * E7 * R) + (K3 * RV)
     
     
     dRp <- (P1u * CD * R) - (P2u * CE * Rp) - (k2 * Rp * E) + 
            (K2 * RpE) - (Pd1u * Rp) + (Pd2u * Rpp)- (k4 * E7 * Rp) + (K4 * RpV)
     

     dRpp <- (P2u * CE * Rp) + (P2c * CE * RpE) - (Pd2u * Rpp) + (Pi2u * CE * RpV) + 
             (Pi2c * CE * RpEV)
     
     
     dRE <- (k1 * R * E) - (K1 * RE) - (P1c * CD * RE) + 
            (Pd1c * RpE) - (k5 * E7 * RE) + (K5 * REV)
     
     dRpE <- (P1c * CD * RE) + (k2 * Rp * E) - (K2 * RpE) - 
             (P2c * CE * RpE) - (Pd1c * RpE) - (k6 * E7 * RpE) + (K6 * RpEV)
     
     dE <- (-k1 * R * E) + (K1 * RE) - (k2 * Rp * E) + (K2 * RpE) +
           (P2c * CE * RpE) + (Pi2c * CE * RpEV) + (K8 * RpEV) +
           (K7 * REV)

     dCE <- (((a2u * E)/(KE + E)) - (de * CE))
     
     dRV <-(k3 * E7 * R) - (K3 * RV) - (Pi1u * CD * RV) + 
            (Pdi1u * RpV ) + (K7 * REV)
     
     dRpV <- (Pi1u * CD * RV) - (Pi2u * CE * RpV )- (Pdi1u * RpV) + 
              ( K8 * RpEV) +( k4 * E7 * Rp) - (K4 * RpV)
     
     dREV <- (-Pi1c * CD * REV) + (Pdi1c * RpEV) + 
              (k5 * E7 * RE) - (K5 * REV ) - (K7 * REV)
     
     dRpEV <- (Pi1c * CD * REV) - (Pi2c * CE * RpEV) - 
               (Pdi1c * RpEV) - (K8 * RpEV ) + (k6 * E7 * RpE) - (K6 * RpEV)
     
     res <-c(dCD, dR, dRE, dRp, dRpp, dRpE, dE, dCE, dRV, dRpV, dREV, dRpEV)
     list(res)
  })
}

uninf  <- ode(y = state, times = times, func = calc.Variables, parms = parms(E7=uninfected))

low.inf <- ode(y = state, times = times, func = calc.Variables, parms = parms(E7=infected))

high.inf <- ode(y = state, times = times, func = calc.Variables, parms = parms(E7=infected, K=highriskK))
```



#3 Results 
In this chapter the results of cell cycle dysregulation due to Human Papillomavirus Infection will be presented. Here a comparison will be made between infected and uninfected cells.  

##3.1 Cycle dysregulation infected and uninfected cells

```{r one, echo=FALSE, fig.width=7,fig.height=6,fig.cap="\\label{fig:one}CE and CD concentrations uninfected and infected state", echo=FALSE}
par(mfrow=c(1,1))
plot(uninf[,2], type="l", xaxt = 'n', ylim = c(0, 0.16), col= 'blue', lwd =2, xlab = "Time (hr)", ylab = "concentration (uM)", 
     main = "Concentration CE and CD where the dotted line \n corresponds with infected state as a function of time")
lines(uninf[,9], col = 'red', lwd = 2)
lines(high.inf[,9], lty = 2, col = 'red')
axis(1, at = c(0,50,100,150,200), labels = c(0,5,10,15,20))
legend(135, 0.15, legend=c("CE" ,"CE infected", "CD"),
       col=c("red", "red", "blue"), lty= c(1,2,1))

```

Figure \ref{fig:one} shows the difference between the concentration of CE(Cyclin E:CDK2) and CE when infected by HPV(human papillomavirus). 
It clearly shows that the CE concentration reaches its' equilibrium sooner than when the cell is uninfected. Despite this, the eventual end concentration of CE is the same for the infected and uninfected cell. CD(Cyclin D:CDK4/6) is shown in this plot because CD activates the cascade of reactions that phosphorylize $RB$. The concentration is steadily increased. According to the presented model CD concentration is independent of whether a cell is infected or not. For this reason only one CD curve is shown.


\newpage

```{r two, echo=FALSE, fig.width=7,fig.height=6,fig.cap="\\label{fig:two}R, Rp and Rpp concentrations corresponding to infected and uninfected states", echo=FALSE}
plot(uninf[,3], type="l", xaxt = 'n', ylim = c(0, 0.45), col= 'red', lwd =2, xlab = "Time (hr)", ylab = "concentration (uM)", 
     main = "Concentration R, RP and Rpp where the dotted line \n corresponds with infected state as a function of time")
lines(uninf[,5], col = 'blue', lwd = 2)
lines(uninf[,6], col = 'green', lwd = 2)

lines(high.inf[,3], col = 'red', lwd = 2, lty = 2)
lines(high.inf[,5], col = 'blue', lwd = 2, lty = 2)
lines(high.inf[,6], col = 'green', lwd = 2, lty = 2)

axis(1, at = c(0,50,100,150,200), labels = c(0,5,10,15,20))
legend(150, 0.4, legend=c("R", "Rp", "Rpp","R inf", "Rp inf", "Rpp inf"),
       col=c("red", "blue", "green"), lty= rep(c(1,2), each = 3))

```

Figure \ref{fig:two} shows the differnce between several states when the cell is infected versus when the cell is uninfected. The states shown are R(Unphosphorylated $RB$), Rp(Hypophosphorylated $RB$) and Rpp(Hyperphosphorylated $RB$). All the lines in the plot show a simmilar correlation, the infected variant of the state reaches its' peak or trough sooner than the uninfected variant, but eventually the two curves will reach the same equilibrium. These curves also start their climb or fall sooner than the uninfected cells.

\newpage

```{r three, echo=FALSE, fig.width=7,fig.height=6,fig.cap="\\label{fig:three}R, Rp and Rpp concentrations for uninfected and infected cells", echo=FALSE}
plot(uninf[,4], type="l", xaxt = 'n', ylim = c(0, 0.25), col= 'red', lwd =2, xlab = "Time (hr)", ylab = "concentration (uM)", 
     main = "Concentration RE, RpE and E where the dotted line \n corresponds with infected state as a function of time")
lines(uninf[,7], col = 'blue', lwd = 2)
lines(uninf[,8], col = 'green', lwd = 2)

lines(high.inf[,4], col = 'red', lwd = 2, lty = 2)
lines(high.inf[,7], col = 'blue', lwd = 2, lty = 2)
lines(high.inf[,8], col = 'green', lwd = 2, lty = 2)

axis(1, at = c(0,50,100,150,200), labels = c(0,5,10,15,20))
legend(150, 0.18, legend=c("RE", "RpE", "E", "RE inf", "RpE inf", "E inf"),
       col=c("red", "blue", "green"), lty= rep(c(1,2), each=3))

```

Figure \ref{fig:three} showns the differnce between several states when the cell is infected versus when the cell is uninfected. The states shown are RE(Unphosphorylated $RB$:E2F), RpE(Hypophosphorylated $RB$p:E2F) and E(E2F). The curves roughly follow the same path untill a certain time is reached. At this time the curves descend(in the case of RpE and RE) or ascend(in the case of E). Once again the equilibrium is independent of whether the state is infected or not.

\newpage

```{r four, echo=FALSE, fig.width=7,fig.height=6,fig.cap="\\label{fig:four}R, Rp and Rpp concentrations as function of time", echo=FALSE}
plot(high.inf[,10], type="l", xaxt = 'n', ylim = c(0, 0.055), col= 'red', lwd =2, xlab = "Time (hr)", ylab = "concentration (uM)", main = "Concentration Rv, RpV, REV and RpEV as a function of time")
lines(high.inf[,11], col = 'blue', lwd = 2)
lines(high.inf[,12], col = 'green', lwd = 2)
lines(high.inf[,13], col = 'purple', lwd = 2)
axis(1, at = c(0,50,100,150,200), labels = c(0,5,10,15,20))
legend(160, 0.05, legend=c("Rv", "RpV", "REV", "RpEV"),
       col=c("red", "blue", "green", "purple"), lty= 1)
```

Figure \ref{fig:four} shows the concentration of Rv(Unphosphorylated $RB$:E7), RpV(Hypophosphorylated $RB$p:E7), REV(Unphosphorylated $RB$:E2F:E7) and RpEV(Hypophosphorylated $RB$p:E2F:E7) as a function of time. All of these values only form when the cell is infected, due to E7 being present in the cell. Therefore the plotted values will be zero at a constant rate when the cell is uninfected. HPV will secreet E7, E7 will that assist in the formation of these parameters. After a while the concentrations will reach an equilibrium of 0.00 uM/h.

\newpage

```{r five, echo=FALSE, fig.width=7,fig.height=6,fig.cap="\\label{fig:five} G1/S transition time varies as function of growth factor (GF) and E7 concentrations", echo=FALSE}

g <- seq(0,100,25)

trans.time <- function (E7){
  test <- vector()
  trans.value <- 0.18
  val <- seq(0, 0.10,0.02)
  for(value in val){
    high.infected <- as.data.frame(ode(y = state, times = times, func = calc.Variables, parms = parms(GF=value, E7=E7, K=highriskK)))
    item <- high.infected$time[high.infected$E > trans.value][1]
    test <- append(test, item, after = length(test))
  }
  return(test)
}


HR.E7 <- trans.time(infected)
LR.E7 <- trans.time(low.infected)
NO.E7 <- trans.time(uninfected)

plot(HR.E7[2:length(HR.E7)], g, type = 'l', xlim = c(0,20), col = 'red', xlab = 'GF 10^-5 uM', ylab = "G1/S transition time per hour", main = "G1/s transition as function of growth factor\n for different E7 concentration")
lines(NO.E7[2:length(NO.E7)])
lines(LR.E7, col = 'blue')
legend(15, 90, legend=c("HR E7", "NO E7", "LR E7"),
       col=c("red", "black", "blue"), lty= 1)

```

Figure \ref{fig:five} displays the G1/s transition time that varies as function of growth factor and $E7$ concentration. For high risk E7 when $GF$ is lower than approximately 7 the transition time is longer than 100 hours. When $GF$ exceeds 7 the cell starts to divide. The time it takes for the cell to divide decreases quickly with increasing $GF$ concentration. Low-risk E7 and NO E7 start dividing when $GF$ is added and is approximately is 1 

\newpage

#4 Conclusion and discussion 

Figure \ref{fig:one} shows the difference between the concentration of CE(Cyclin E:CDK2) and CE when infected by HPV(human papillomavirus). In this figure uninfected CEshows a sharp increase that stabilizes at a concentration of 0.16 uM. The same concentration will be reached when the cell is infected after five hours. Here activation of Cyclin E (CE) will be activated prematurally. Binding of E7 to $RB$ will cause the release of E2F that increases the production of CE. The concentration CD will be unaffected in infected or uninfected cells. Reason for this is that CD, as stated in earlier in the section Estimates and assumptions, CDKs are available in excess compared to cyclins$^8$. 

Figure \ref{fig:two} The amount of unphosphorylated R decreases over time and around ten hours equilibrium is reached. During this time frame unphosphorylated R is being phosphorylated into Rp and further into Rpp. After phosphorylated R is created the compount is phosphorylated again to form Rpp. The sharp decrease and increase of Rp at the start display the proces of phosphorylation of R into Rp. After ten hours phosphorylation has stopped and all R and Rp has sequentially been phosphorylated into Rpp. When the cell is infected with HPV this proces, displayed by the solid lines, happens faster. The article Differential regulation of E2F and Sp1-mediated transcription by G1 cyclins states  activation processes by phosphorylation inactivates $RB$ that allows cell cycle progression$^{18}$

Progression of the substances displayed in figure \ref{fig:three} show a similair progression with Figure \ref{fig:two}. The amount of RE decreases sharply during the first five hours because RE is being phosphorylated into RpE When phosphorylation into Rpe takes place. The decrease of RpE around 5 hours indicates phosphorylation of RpE into hyperphosphorylated $RB$. The solid lines, indicating infection by HPV, show that this process takes place faster. 

Figure \ref{fig:four} show concentration of compounds that interact with E7 protein formed by HPV. Phospobrylated $RB$:E7 complex. RV and RpV increases almost simultaneously due to the amount of phosphorylated and unphosphorylated $RB$ present in the cell. $RB$ continues to be progressively phosphorylated, but there is less Rp due to the additional states of RpV and RpEV.

The last figure, figure \ref{fig:five}, shows the G1/S transition time varies as function of growth factor (GF) and E7 concentrations. The plot shown does not appear to be correct. The figure in the article is significantly different. In The appendix the actual plot is given. The article states the following about calculation of G1/s transition Time that varies as function of growth factor (GF) and E7 concentrations: 

*'We examine how the G1/S transition time varies as a function of the growth factor (GF) and E7 concentrations. We define transition time as the time it takes E2F to reach 90% of its maximum concentration (Etot), and if the model predicts that it takes over 100 h to transition, we assume that the cell does not commit to cell division.'*$^2$

Etot is defined as the total amount of E2F given this is in total 0.2 uM. It has been calculated at what time 0.2 uM E2F reached 90% of its total, this is at 0.18 uM. For a different amount of E7 and a different amount of growth factor (varying from 0 to 10) these time points have been calculated and displayed in the graph. The HR E7 show when more than 7 GF is added the transition time sharply decreases. NO E7 and LR E7 show a different line of progression where adding GF concentration has barely any effect on the transition time. Furthermore no equilibrium occurs. 

It can be concluded that high $RB$ binding affinity and E7 concentration accelerate the G1/S phase transition. The models shown in the figure clearly show a time decrease in which biological processes influence cell cycle regulation on multiple levels. In all the described figures the time for infected cells is decreased. 
Weakening the dependence on growth factor due to HPV could not be proven. Calculating depedency of growth factor for different amount of E7 is likely to be interpreted incorrectly. Due to the limited time of this research project further analysis has been ceased. The article A Mathematical Model of Cell Cycle Dysregulation Due to Human Papillomavirus Infection mentions no further information to proceed to get a different outcome. 


\newpage
# References

[1] Rijksinstituut voor Volksgezondheid en Milieu (RIVM) ,20-05-2019, https://www.rivm.nl/hpv-humaan-papillomavirus

[2] Anna K. Miller, Karl Munger, Frederick R. Adler, 12-06-2017, https://link.springer.com/article/10.1007/s11538-017-0299-9#Equ1

[3] Johnson DG, Schwarz JK, Cress WD, Nevins JR (1993) Expression of transcription factor E2F1 induces quiescent cells to enter S phase. Nature 365(6444):349–352

[4] Henley SA, Dick FA (2012) The retinoblastoma family of proteins and their regulatory functions in the mammalian cell division cycle. Cell Div 7:10

[5] Soetaert K, Petzoldt T, Setzer RW (2010) Solving differential equations in R: package deSolve. J Stat Softw 33(9):1–25, http://www.jstatsoft.org/v33/i09

[6] Ekholm SV, Reed SI (2000) Regulation of G1G1 cyclin-dependent kinases in the mammalian cell cycle. Curr Opin Cell Biol 12(6):676–684 

[7] Weinberg R (2013) The biology of cancer. Garland Science, New York 

[8] Morgan DO (2007) The cell cycle: principles of control. New Science Press, London

[9] Ludlow J, Glendening C, Livingston D, DeCarprio J (1993) Specific enzymatic dephosphorylation of the retinoblastoma protein. Mol Cell Biol 
13(1):367–372

[10] Won KA, Reed SI (1996) Activation of cyclin E/CDK2 is coupled to site-specific autophosphorylation and ubiquitin-dependent degradation of cyclin E. EMBO J 15(16):4182–4193

[11] Park S, Chung S, Kim KM, Jung KC, Park C, Hahm ER, Yang CH (2004) Determination of binding constant of transcription factor myc-max/max-max and E-box DNA: the effect of inhibitors on the binding. Biochim Biophys Acta 1670(3):217–228

[12] Aoki K, Yamada M, Kunida K, Yasuda S, Matsuda M (2011) Processive phosphorylation of ERK MAP kinase in mammalian cells. Proc Natl Acad Sci 108(31):12,675–12,680

[13] Lee C, Chang JH, Lee HS, Cho Y (2002) Structural basis for the recognition of the E2F transactivation domain by the retinoblastoma tumor suppressor. Genes Dev 16(24):3199–3212

[14] Chemes LB, Sánchez IE, de Prat-Gay G (2011) Kinetic recognition of the retinoblastoma tumor suppressor by a specific protein target. J Mol Biol 412(2):267–284

[15] Wu EW, Clemens K, Heck D, Münger K (1993) The human papillomavirus E7 oncoprotein and the cellular transcription factor E2F bind to separate sites on the retinoblastoma tumor suppressor protein. J Virol 67(4):2402–2407

[16] Fujioka A, Terai K, Itoh RE, Aoki K, Nakamura T, Kuroda S, Nishida E, Matsuda M (2006) Dynamics of the Ras/ERK MAPK cascade as monitored by fluorescent probes. J Biol Chem 281(13):8917–8926

[17] Soetaert K, Petzoldt T, Setzer RW (2010) Solving differential equations in R: package deSolve. J Stat Softw 33(9):1–25, http://www.jstatsoft.org/v33/i09

[18] Shao Z, Robbins PD (January 1995). Differential regulation of E2F and Sp1-mediated transcription by G1 cyclins. Oncogene. 10 (2): 221–8. PMID 7838522 
